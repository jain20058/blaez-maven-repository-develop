package test;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import test.base.Page;

public class LoginPage extends Page {

	//passing the website url in the browser
	public void OpenWebsite(){	  
		driver.get(CONFIG.getProperty("Website_URL")); 		
	}	

	//Close Browser
	public void CloseBrowser(){
		driver.quit();
	}

	public void SignIn() {
		InputXpath(OR.getProperty("Email"), CONFIG.getProperty("Email_Id"));
		InputXpath(OR.getProperty("Password"), CONFIG.getProperty("Password"));
		ClickXpath("Login");
	}

	public void Logout() {
		ClickXpath("LogOutDropDown");
		ClickXpath("LogOut");
	}
}
