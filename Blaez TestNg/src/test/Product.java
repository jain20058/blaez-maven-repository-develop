package test;

import org.testng.Assert;

import test.base.Page;

public class Product extends Page{

	public void NavigateProduct(String Product) {
		ClickXpath(Product);
		
	}
	
	public void NewProduct(){
		ClickXpath("NewProduct");
	}

	public void AddProductProperties(String CurrentDate) {
		Input(OR.getProperty("AddProductName"), CurrentDate);
		Input(OR.getProperty("AddProductBrand"), CONFIG.getProperty("Brand"));
		Input(OR.getProperty("AddProductCategory"), CONFIG.getProperty("Category"));
		Input(OR.getProperty("AddProductPrice"), CONFIG.getProperty("Price"));
		Input(OR.getProperty("AddproductTax"), CONFIG.getProperty("Tax"));
		Input(OR.getProperty("AddProductLowThreshold"), CONFIG.getProperty("LowThreshold"));
		Input(OR.getProperty("AddProductAvailableQuantity"), CONFIG.getProperty("AvailableQuantity"));
		Input(OR.getProperty("AddProductMinOrderValue"), CONFIG.getProperty("MinOrderQuantity"));
		Input(OR.getProperty("AddProductUnitValue"), CONFIG.getProperty("UnitValue"));
		ValueFromDropdownVisibleTextId("AddProductUnit", CONFIG.getProperty("Unit"));
		ClickXpath("AddProductSave");
	}

	public void VerifyNewProduct(String CurrentDate) {
		ClickXpath("FirstProductName");	
		//Assert.assertTrue(IsTextPresentXpath("FirstProductName", CurrentDate));		
	}

	public void SearchProduct(String CurrentDate) {
		InputXpath(OR.getProperty("ProductSearch"), CurrentDate);		
	}

	public void ClickProductName() {
		ClickXpath("FirstProductName");		
	}

	public void EditProduct(String CurrentDate) throws InterruptedException {
		ClearField(OR.getProperty("AddProductName"));
		Input(OR.getProperty("AddProductName"), CurrentDate);
		ClearField(OR.getProperty("AddProductBrand"));
		ClearField(OR.getProperty("AddProductCategory"));
		ClearField(OR.getProperty("AddProductPrice"));
		ClearField(OR.getProperty("AddproductTax"));
		ClearField(OR.getProperty("AddProductLowThreshold"));
		ClearField(OR.getProperty("AddProductAvailableQuantity"));
		ClearField(OR.getProperty("AddProductMinOrderValue"));
		ClearField(OR.getProperty("AddProductUnitValue"));		
		Input(OR.getProperty("AddProductBrand"), CONFIG.getProperty("BrandNew"));
		Input(OR.getProperty("AddProductCategory"), CONFIG.getProperty("CategoryNew"));
		Input(OR.getProperty("AddProductPrice"), CONFIG.getProperty("PriceNew"));
		Input(OR.getProperty("AddproductTax"), CONFIG.getProperty("TaxNew"));
		Input(OR.getProperty("AddProductLowThreshold"), CONFIG.getProperty("LowThresholdNew"));
		Input(OR.getProperty("AddProductAvailableQuantity"), CONFIG.getProperty("AvailableQuantityNew"));
		Input(OR.getProperty("AddProductMinOrderValue"), CONFIG.getProperty("MinOrderQuantityNew"));
		Input(OR.getProperty("AddProductUnitValue"), CONFIG.getProperty("UnitValueNew"));
		ValueFromDropdownVisibleTextId("AddProductUnit", CONFIG.getProperty("UnitNew"));
		Thread.sleep(5000);
		ClickXpath("AddProductSave");
	}
	
	public void UpdateLowThreshold() throws InterruptedException{
		ClearFieldXpath(OR.getProperty("FirstProductLowThreshold"));
		Thread.sleep(5000);
		InputXpath(OR.getProperty("FirstProductLowThreshold"), CONFIG.getProperty("UpdateLowThreshold"));
		ClickXpath("ProductSearch");
	}

	public void VerifyLowThreshold() {
		Assert.assertTrue(IsValuePresentXpath("FirstProductLowThreshold", CONFIG.getProperty("UpdateLowThreshold")));	
	}

	public void UpdateInventory() throws InterruptedException {
		ClickXpath("Recieve");
		Thread.sleep(5000);
		ClearFieldXpath(OR.getProperty("FirstProductInventory"));
		Thread.sleep(5000);
		InputXpath(OR.getProperty("FirstProductInventory"), CONFIG.getProperty("UpdateInventory"));
		ClickXpath("ProductSearch");
	}

	public void VerifyInventory() {
		Assert.assertTrue(IsValuePresentXpath("FirstProductInventory", CONFIG.getProperty("UpdateInventory")));
	}

	public void Contracts() throws InterruptedException {
		ClickXpath("Contacts");
		Thread.sleep(10000);
		ClickXpath("AddNewContract");
		Input(OR.getProperty("ContactStoreName"), CONFIG.getProperty("Store_Name"));
		ClickXpath("FirstContractStoreName");
		Thread.sleep(5000);
		InputXpath(OR.getProperty("ContractPrice"), CONFIG.getProperty("MaxPrice"));
		ClickXpath("ContractSubmit");
	}

	public void VerifyContract() {
		ClickXpath("Contacts");
		Assert.assertTrue(IsTextPresentXpath("FirstContractStore", CONFIG.getProperty("Store_Name")));
	}
	

}
