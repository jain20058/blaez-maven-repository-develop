package test.java;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import test.*;
import test.base.Page;

public class BlaezTest extends Page{	 

	LoginPage lp = new LoginPage();
	Product Pr = new Product();


	//Add A Product	 
	@Test(priority=1)
	public void AddProduct() throws InterruptedException{  
		System.out.println("Add A Product");   		      
		lp.OpenWebsite();
		lp.SignIn();
		Pr.NavigateProduct("Product");
		Pr.NewProduct();
		Pr.AddProductProperties(CurrentDate1());
		Pr.SearchProduct(CurrentDate1());
		Thread.sleep(5000);
		Pr.VerifyNewProduct(CurrentDate1());
		lp.Logout();
	}

	//Edit The Product
	@Test(priority=2)
	public void EditProduct() throws InterruptedException {
		System.out.println("Edit The Product");
		lp.OpenWebsite();
		lp.SignIn();
		Pr.NavigateProduct("Product");
		Pr.SearchProduct(CurrentDate1());
		Thread.sleep(8000);
		Pr.ClickProductName();
		Thread.sleep(5000);
		Pr.EditProduct(CurrentDate2());
		Thread.sleep(5000);
		Pr.NavigateProduct("Product");
		Pr.SearchProduct(CurrentDate2());
		Thread.sleep(8000);
		Pr.VerifyNewProduct(CurrentDate2());
		lp.Logout();
	}
	
	//Update Low Threshold
	@Test(priority=3)
	public void UpdateLowThreshold() throws InterruptedException{
		System.out.println("Update Low Threshold");
		lp.OpenWebsite();
		lp.SignIn();
		Pr.NavigateProduct("Product");
		Pr.SearchProduct(CurrentDate2());
		Thread.sleep(5000);
		Pr.UpdateLowThreshold();
		Thread.sleep(8000);
		Pr.VerifyLowThreshold();
		lp.Logout();
	}
	
	//Update Inventory
	@Test(priority=4)
	public void UpdateInventory() throws InterruptedException{
		System.out.println("Update Inventory");
		lp.OpenWebsite();
		lp.SignIn();
		Pr.NavigateProduct("Product");
		Pr.SearchProduct(CurrentDate2());
		Thread.sleep(5000);
		Pr.UpdateInventory();
		Thread.sleep(5000);
		Pr.VerifyInventory();
		lp.Logout();
	}
	
	//Add Contracts
	@Test(priority=5)
	public void AddContracts() throws InterruptedException{
		System.out.println("Add Contracts");
		lp.OpenWebsite();
		lp.SignIn();
		Pr.NavigateProduct("Product");
		Pr.SearchProduct(CurrentDate2());
		Thread.sleep(5000);
		Pr.Contracts();
		Thread.sleep(5000);
		Pr.VerifyContract();
		lp.Logout();
	}
	
	//Close Browser
	@AfterTest()
	public void QuitBrowser()
	{
		lp.CloseBrowser();
	}

} 